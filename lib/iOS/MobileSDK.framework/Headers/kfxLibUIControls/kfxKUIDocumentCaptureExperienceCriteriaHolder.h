//
//  kfxKUIDocumentCaptureExperienceCriteriaHolder.h
//  kfxLibUIControls
//
//  Copyright (c) 2017 Kofax. All rights reserved.
//

#import <kfxLibUIControls/kfxKUIDocumentBaseCaptureExperienceCriteriaHolder.h>

@class kfxKEDDocumentDetectionSettings;

//! This class allows the document related criteria to be configured.
/**
 This class allows all of the capture related parameters to be configured.
 */
@interface kfxKUIDocumentCaptureExperienceCriteriaHolder : kfxKUIDocumentBaseCaptureExperienceCriteriaHolder

/// Document detection settings object.
/**
 A collection of values that control the behavior of document detection experience. Pass nil for the experience of default setting.
 */
@property (nonatomic, strong) kfxKEDDocumentDetectionSettings* documentDetectionSettings;

@end
