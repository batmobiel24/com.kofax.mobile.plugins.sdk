//
//  kfxKUICheckCaptureExperienceCriteriaHolder.h
//  kfxLibUIControls
//
//  Copyright (c) 2012 - 2017 Kofax. Use of this code is with permission pursuant to Kofax license terms.
//

#import <kfxLibUIControls/kfxKUIDocumentBaseCaptureExperienceCriteriaHolder.h>

@class kfxKEDCheckDetectionSettings;

//! This class allows the check related criteria to be configured.
/**
 This class allows all of the capture related parameters to be configured.
 */
@interface kfxKUICheckCaptureExperienceCriteriaHolder : kfxKUIDocumentBaseCaptureExperienceCriteriaHolder

/// Check detection settings object.
/**
 A collection of values that control the behavior of check detection experience. Pass nil for the experience of default setting.
 */
@property (nonatomic, retain) kfxKEDCheckDetectionSettings* checkDetectionSettings;

@end
