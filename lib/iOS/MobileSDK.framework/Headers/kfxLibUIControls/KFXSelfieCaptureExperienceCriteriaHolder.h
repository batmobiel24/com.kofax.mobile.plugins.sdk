//
//  kfxSelfieExperienceCriteriaHolder.h
//
//  Created by Kofax on 18/01/17.
//  Copyright © 2017 Kofax. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <kfxLibUIControls/KFXSelfieDetectionSettings.h>

@interface KFXSelfieCaptureExperienceCriteriaHolder : NSObject

/// Selfie detection settings object.
/**
 A collection of values that control the behavior of selfie detection experience. Pass nil for the experience of default setting.
 */
@property (nonatomic) KFXSelfieDetectionSettings* selfieDetectionSettings;


@end
