# Kofax Phonegap Plugin

Please do following changes inorder to work this plugin for OutSystems.

## For Android

1. Copy the lib files from **KofaxMobileSDK(Android/MobileSDK_libs/aar)** to plugin **(lib/Android)**.
2. Comment the below lines of code in **plugin.xml**

```
<lib-file src="lib/Android/okhttp-3.10.0.jar" />
<lib-file src="lib/Android/gson-2.8.2.jar" />
<lib-file src="lib/Android/okio-1.14.0.jar" />
<framework src="com.android.support:support-v4:27.1.1" />
```

## For iOS

1. Copy the frameworks from **KofaxMobileSDK(iOS/Frameworks/MobileSDK)** to plugin **(lib/iOS)**.

## Final Steps
After doing above changes to plugin for both iOS and Android platforms, add it to the project and try to use.
